import Phaser from 'phaser';
import TextPlayerPlugin from 'phaser3-rex-plugins/plugins/textplayer-plugin.js';
import Game from './Game';

export default {
	width: 800,
	height: 600,
	title: "Coba Phaser Baru",
	pixelArt: true,
	url: "https://github.com/mpratama",
	backgroundColor: 0x000000,
	scale: {
		mode: Phaser.Scale.FIT,
		autoCenter: Phaser.Scale.CENTER_BOTH
	},
	physics: {
		default: 'arcade',
		arcade: {
			//gravity: { y: 100 },
			debug: false
			}
	},
	plugins: {
     global: [{
         key: 'rexTextPlayerPlugin',
         plugin: TextPlayerPlugin,
         start: true
     },
     // ...
     ]
    },
	scene: [Game]
};