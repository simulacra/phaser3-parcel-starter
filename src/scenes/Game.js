import Phaser from 'phaser';
import bg from '../assets/world.png';

export default class Game extends Phaser.Scene {
	constructor(){
		super({key: "gameScene"});
	}

	preload(){
	    this.load.image("gambar", bg)
	}
	
    create() {
            var content = `\
    Phaser is a fast, free, and fun open source HTML5 game framework that offers WebGL and Canvas rendering across desktop and mobile web browsers. Games can be compiled to iOS, Android and native apps by using 3rd party tools. You can use JavaScript or TypeScript for development.\
    `;

            var text = this.add.rexTextPlayer(
                {
                    x: 400, y: 300,
                    width: 400, height: 200,  // Fixed width and height

                    background: {
                        stroke: 'white',
                        cornerRadius: 20
                    },

                    innerBounds: {
                        stroke: '#A52A2A'
                    },

                    padding: 20,

                    style: {
                        fontSize: '10px',
                        stroke: 'green',
                        strokeThickness: 3,

                        shadowColor: 'red',
                        shadowOffsetX: 5,
                        shadowOffsetY: 5,
                        shadowBlur: 3
                    },

                    wrap: {
                        maxLines: 5,
                        padding: { bottom: 10 },
                    },

                    typing: {
                        speed: 250,  // 0: no-typing

                    },

                    // ignoreNextPageInput: true,  // or text.setIgnoreNextPageInput()
                    clickTarget: this,
                    nextPageInput: 'click|2000'

                }
            )
                .setTypingSpeed(5)
                .setIgnoreNextPageInput()


            var print = this.add.text(0, 580, 'Click to start');
            this.input.once('pointerdown', function () {
                text.playPromise(content)
                    .then(function () {
                        print.setText('Play complete');
                        console.log('Play complete');
                    })
            })
        }
}