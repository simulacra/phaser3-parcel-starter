# phaser3-parcel-starter

*Starter template* **(2D)** **(Javascript)** game development menggunakan phaser3 framework dan parceljs bundler.

## Setup
1. Install parcel bundler global:
`npm install -g parcel`
2. Instal dependency
`npm i`

## Documentation
- [Phaser3 API documentation](https://newdocs.phaser.io/docs "Phaser3 API documentation")
- [Phaser 3 examples](https://phaser.io/examples "Phaser 3 examples")
- [rex-rainbow-notes plugin list](https://rexrainbow.github.io/phaser3-rex-notes/docs/site/plugin-list/ "rex-rainbow-notes plugin list")
- [Parceljs documentation](https://parceljs.org/docs/ "Parceljs documentation")

## Development
`npm run dev`

Lalu buka web browser pada localhost:1234.

## Production
`npm run build`
